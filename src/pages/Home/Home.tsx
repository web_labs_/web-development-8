import { useEffect } from 'react';
import { AppRoutes } from '../../common/constants';
import './index.css';

export function Home() {
  /**
   * useState для постов
   * useState для чтобы показать грузятся посты или нет
   */

  const fetchPosts = async () => {
    setIsLoading(true);

    /**
     * const posts = await API CALL
     * setPosts(posts);
     */

    setIsLoading(false);
  };

  useEffect(() => {
    /**
     * загружаем посты на первой загрузке страницы
     */
  }, []);

  const renderPosts = () => {
    if (isLoading) {
      return null;
    }

    return posts.map((post) => (
      <div className="post-container" key={post.id}>
        <a href={AppRoutes.POST(post.id)} className="post-title">
          {post.title}
        </a>
        <small>{post.body}</small>
      </div>
    ));
  };

  return (
    <div className="App">
      <main className="posts">{renderPosts()}</main>
    </div>
  );
}
